﻿using System;
using System.Collections.Generic;
using System.Text;

namespace AnimalCoopSim.Models
{
    public class AnimalKindDetails
    {
        public int Id { get; set; }
        public string Kind { get; set; }
        public int BornCountAtOnce { get; set; }
        public int FemaleBornPercent { get; set; }
        public int DurationOfPregnancy { get; set; }
        public int FemaleLifeTime { get; set; }
        public int MaleLifeTime { get; set; }
        public int MinAgeForFertility { get; set; }
      
    }
}


