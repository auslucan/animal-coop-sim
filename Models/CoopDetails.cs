﻿using System;
using System.Collections.Generic;
using System.Text;

namespace AnimalCoopSim.Models
{
    public class  CoopDetails
    {
        public int NewBornMaleCount { get; set; }
        public int NewBornFemaleCount { get; set; }
        public List<int> NewFemaleDeaths { get; set; } = new List<int>();
        public List<int> NewMaleDeaths { get; set; } =new List<int>();

        public List<Male> Males { get; set; } = new List<Male>();
        public List<Female> Females { get; set; } = new List<Female>();

        public int TotalMaleCount { get; set; }
        public int TotalFemaleCount { get; set; }
        public int TotalCoopAnimalCount => TotalMaleCount + TotalFemaleCount- TotalDeathsCount;
        public List<int> TotalFemaleDeaths { get; set; }
        public List<int> TotalMaleDeaths { get; set; }
        public int TotalDeathsCount { get; set; }


    }
}
