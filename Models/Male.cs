﻿using AnimalCoopSim.Enums;
using System;
using System.Collections.Generic;
using System.Text;

namespace AnimalCoopSim.Models
{
    public class Male
    {
        public int Id { get; set; }
        public Gender Gender { get; set; }
        public int Age { get; set; }
        public int LifeTime { get; set; }

    }
}
