﻿using AnimalCoopSim.Enums;
using System;
using System.Collections.Generic;
using System.Text;

namespace AnimalCoopSim.Models
{
    public class  Female
    {
        public int Id { get; set; }
        public Gender Gender { get; set; }
        public int Age { get; set; }
        public int MonthOfPregnancy { get; set; }
        public int DurationOfPregnancy { get; set; }
        public int LifeTime { get; set; }
        public int MinAgeForFertility { get; set; }
        public bool IsPregnant ()=> MonthOfPregnancy > 0;
        public bool IsReadyForPregnancy() => (Age >= MinAgeForFertility) && !IsPregnant();
        public bool IsFemale() => Gender == Gender.Female;
        public bool IsMale() => Gender == Gender.Male;

    }
}
