﻿using AnimalCoopSim.Enums;
using AnimalCoopSim.Events;
using AnimalCoopSim.Interfaces;
using AnimalCoopSim.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace AnimalCoopSim.Managers
{
    public class CoopManager: ICoopManager
    {
        public AnimalKindDetails _animalKindDetails;
        public event EventHandler<CoopEventArg> CoopProcess;

        public CoopManager(AnimalKindDetails kindDetails)
        {
            new Logger().Subscribe(this);

            _animalKindDetails = new AnimalKindDetails()
            {
                Kind = kindDetails.Kind,
                BornCountAtOnce = kindDetails.BornCountAtOnce,
                DurationOfPregnancy = kindDetails.DurationOfPregnancy,
                FemaleBornPercent = kindDetails.FemaleBornPercent,
                FemaleLifeTime = kindDetails.FemaleLifeTime,
                MaleLifeTime = kindDetails.MaleLifeTime,
                MinAgeForFertility = kindDetails.MinAgeForFertility
            };
        }
   
        public void RemoveFemaleDeathMemberFromCoop(List<int> members, ref List<Female> females)
        {

            females = females.Except(females.Where(q => members.Any(a => a == q.Id))).ToList();
        }
        public void RemoveMaleDeathMemberFromCoop(List<int> members, ref List<Male> males)
        {
            males = males.Except(males.Where(q => members.Any(a => a == q.Id))).ToList();
        }
        public void AddNewFemaleBornToCoop(int newBornCount, ref List<Female> list)
        {
            for (int i = 0; i < newBornCount; i++)
            {
                list.Add(new Female
                {
                    Gender = Gender.Female,
                    DurationOfPregnancy = _animalKindDetails.DurationOfPregnancy,
                    Id = list.LastOrDefault()?.Id + 1 ?? 1,
                    LifeTime = _animalKindDetails.FemaleLifeTime,
                    MinAgeForFertility=_animalKindDetails.MinAgeForFertility
                });
            }
            CoopProcess(this, new CoopEventArg(_animalKindDetails.Kind, Gender.Male, newBornCount));

        }
        public void AddNewMaleBornToCoop(int newBornCount, ref List<Male> list)
        {
            for (int i = 0; i < newBornCount; i++)
            {
                list.Add(new Male
                {
                    Gender = Gender.Male,
                    Id = list.LastOrDefault()?.Id + 1 ?? 1,
                    LifeTime = _animalKindDetails.MaleLifeTime,
                });
            }
            CoopProcess(this, new CoopEventArg(_animalKindDetails.Kind, Gender.Female, newBornCount));
        }
        public CoopDetails ProcessOfCoopAnimal(List<Female> Females, List<Male> Males, bool increaseAge)
        {
            CoopDetails coopDetails = new CoopDetails();
            foreach (var male in Males)
            {
                if (male.Age > male.LifeTime)
                {
                    coopDetails.NewMaleDeaths.Add(male.Id);
                    continue;
                }
                if (increaseAge)
                {
                    male.Age++;
                }
            }
            foreach (var female in Females)
            {
                if (female.Age > female.LifeTime)
                {
                    coopDetails.NewFemaleDeaths.Add(female.Id);
                    continue;
                }
                if (increaseAge)
                {
                    female.Age++;
                }
                if (female.IsReadyForPregnancy())
                {
                    female.MonthOfPregnancy++;
                }
                else if (female.IsPregnant() && female.MonthOfPregnancy >= female.DurationOfPregnancy)
                {
                    coopDetails.NewBornFemaleCount += CalculateNewBornFemaleCount();
                    coopDetails.NewBornMaleCount += CalculateNewBornMaleCount();
                    female.MonthOfPregnancy = 0;
                }
            }
            return coopDetails;
        }

        public int  CalculateNewBornFemaleCount()
        {
            int femaleNewBorn = Convert.ToInt32(_animalKindDetails.BornCountAtOnce * ((double)_animalKindDetails.FemaleBornPercent / 100));
   
            return femaleNewBorn;
        }
        public int CalculateNewBornMaleCount()
        {
            int maleNewBorn = Convert.ToInt32(_animalKindDetails.BornCountAtOnce * ((double)(100 - _animalKindDetails.FemaleBornPercent) / 100));

            return maleNewBorn;
        }
      
        public (Female femails, Male males) FirstStartingOfSimulation()
        {
            Female femail = new Female
            {
                Id = 1,
                Age = _animalKindDetails.MinAgeForFertility,
                Gender = Gender.Female,
                LifeTime = _animalKindDetails.FemaleLifeTime,
                DurationOfPregnancy = _animalKindDetails.DurationOfPregnancy,
                MinAgeForFertility= _animalKindDetails.MinAgeForFertility
            };
            Male male = new Male { Id = 1, Age = _animalKindDetails.MinAgeForFertility, Gender = Gender.Male, LifeTime = _animalKindDetails.MaleLifeTime };
            return (femail, male);
        }
    }
}
