﻿using AnimalCoopSim.Managers;
using System;
using System.Collections.Generic;
using System.Text;

namespace AnimalCoopSim.Events
{
    public class Logger
    {
        public void Subscribe(CoopManager coop)
        {
            coop.CoopProcess += NewBornLog;
        }

        private void NewBornLog(object sender, CoopEventArg e)
        {
            Console.WriteLine($"New  {e.Count} {e.Gender} {e.Name} s borned");
        }
    }
}
