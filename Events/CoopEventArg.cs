﻿using AnimalCoopSim.Enums;
using System;
using System.Collections.Generic;
using System.Text;

namespace AnimalCoopSim.Events
{
    public class CoopEventArg : EventArgs
    {
        public string Name { get; set; }
        public Gender Gender { get; set; }
        public int Count { get; set; }

        public CoopEventArg(string name, Gender gender, int count)
        {
            Name = name;
            Gender = gender;
            Count = count;
        }
    }
}
