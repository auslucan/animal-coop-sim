﻿using AnimalCoopSim.Enums;
using AnimalCoopSim.Events;
using AnimalCoopSim.Interfaces;
using AnimalCoopSim.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AnimalCoopSim
{
    public class CoopSimulate
    {
        private const int Thread = 100;
        private ICoopManager _coopManager;
        public CoopSimulate(ICoopManager coopManager)
        {
            _coopManager = coopManager;
        }

        public void StartSimulate(int coopLifeDuration)
        {

            (Female firstFemail, Male firstMale) = _coopManager.FirstStartingOfSimulation();
            CoopDetails coopDetails = new CoopDetails() { TotalFemaleCount = 1, TotalMaleCount = 1 };

            List<Female> females = coopDetails.Females;
            List<Male> males = coopDetails.Males;

            coopDetails.Males.Add(firstMale);
            coopDetails.Females.Add(firstFemail);

            for (int time = 1; time <= coopLifeDuration; time++)
            {
                coopDetails.NewBornFemaleCount = 0;
                coopDetails.NewBornMaleCount = 0;
                coopDetails.NewFemaleDeaths = new List<int>();
                coopDetails.NewMaleDeaths = new List<int>();
                var threadLimit = (double)coopDetails.TotalCoopAnimalCount / Thread;
                if (threadLimit == 0)
                    threadLimit = 1;

                var taskList = new List<Task<CoopDetails>>();

                for (int i = 0; i < threadLimit; i++)
                {
                    var femaleList = females.Skip(i * Thread).Take(Thread).ToList();
                    var malesList = males.Skip(i * Thread).Take(Thread).ToList();
                    taskList.Add(Task.Run(() => _coopManager.ProcessOfCoopAnimal(femaleList, malesList, time > 1)));
                }
                Task.WaitAll(taskList.ToArray());

                foreach (var task in taskList)
                {
                    coopDetails.NewBornFemaleCount += task.Result.NewBornFemaleCount;
                    coopDetails.NewBornMaleCount += task.Result.NewBornMaleCount;

                    coopDetails.NewFemaleDeaths.AddRange(task.Result.NewFemaleDeaths);
                    coopDetails.NewMaleDeaths.AddRange(task.Result.NewMaleDeaths);
                }


                _coopManager.AddNewFemaleBornToCoop(coopDetails.NewBornFemaleCount, ref females);
                _coopManager.AddNewMaleBornToCoop(coopDetails.NewBornMaleCount, ref males);

                _coopManager.RemoveFemaleDeathMemberFromCoop(coopDetails.NewFemaleDeaths, ref females);
                _coopManager.RemoveMaleDeathMemberFromCoop(coopDetails.NewMaleDeaths, ref males);

                coopDetails.TotalDeathsCount += coopDetails.NewFemaleDeaths.Count + coopDetails.NewMaleDeaths.Count;
                coopDetails.TotalFemaleCount += coopDetails.NewBornFemaleCount;
                coopDetails.TotalMaleCount += coopDetails.NewBornMaleCount;

            }
            Console.WriteLine($"*** End of the {coopLifeDuration} result ***");
            Console.WriteLine($"During coop life the number of females {coopDetails.TotalFemaleCount} and males are {coopDetails.TotalMaleCount}. And {coopDetails.TotalDeathsCount} members died. \r\n Current Population: {coopDetails.TotalCoopAnimalCount}");
        }

    }
}
