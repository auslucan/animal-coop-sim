﻿using AnimalCoopSim.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace AnimalCoopSim.Interfaces
{
    public interface ICoopManager
    {
        void RemoveFemaleDeathMemberFromCoop(List<int> members, ref List<Female> females);
        void RemoveMaleDeathMemberFromCoop(List<int> members, ref List<Male> males);
        void AddNewFemaleBornToCoop(int newBornCount, ref List<Female> list);
        void AddNewMaleBornToCoop(int newBornCount, ref List<Male> list);
        CoopDetails ProcessOfCoopAnimal(List<Female> Females, List<Male> Males, bool increaseAge);
        int CalculateNewBornFemaleCount();
        int CalculateNewBornMaleCount();

        (Female femails, Male males) FirstStartingOfSimulation();
    }
}
