﻿using AnimalCoopSim.Interfaces;
using AnimalCoopSim.Managers;
using AnimalCoopSim.Models;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;

namespace AnimalCoopSim
{
    class Program
    {
        private readonly static string settingsFile = "appsettings.json";
        static void Main(string[] args)
        {
            var configuration = new ConfigurationBuilder()
                   .AddJsonFile(settingsFile, true, true)
                   .Build();

            List<AnimalKindDetails> animals = configuration.GetSection("AnimalKindDetails").Get<List<AnimalKindDetails>>();

            (int lifeTime, int selection) = GetSelections(animals);
            var selectedAnimal = animals.Where(t => t.Id == selection).FirstOrDefault();


            ServiceProvider serviceProvider = new ServiceCollection()
                                          .AddTransient<ICoopManager>(i => new CoopManager(selectedAnimal))
                                           .BuildServiceProvider();

            ICoopManager coopManager = serviceProvider.GetService<ICoopManager>();

            CoopSimulate coopSimulate = new CoopSimulate(coopManager);
            coopSimulate.StartSimulate(lifeTime);

            Console.ReadLine();
        }

        public static (int, int) GetSelections(List<AnimalKindDetails> animals)
        {
            int selection;
            Console.WriteLine("Please select your animals from list ");
            Console.WriteLine("Number\t Name ");
            Console.WriteLine("----------------");
            animals.ForEach(i => Console.WriteLine("{0}\t {1}", i.Id, i.Kind));

            while (!int.TryParse(Console.ReadLine(), out selection) || !animals.Any(t => t.Id == selection))
            {
                Console.WriteLine("You must select a number from list.. Please enter again: ");
            }

            int lifeTime;
            Console.WriteLine("Please enter lifetime of coop simulation ");

            while (!int.TryParse(Console.ReadLine(), out lifeTime))
            {
                Console.WriteLine("You must select a number from list.. Please enter again: ");
            }
            return (lifeTime, selection);
        }
    }
}
