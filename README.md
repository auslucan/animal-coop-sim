Bir kümesteki hayvanların belirli bir süre içerisindeki yaşam döngülerini simüle eder.

Başlangıçta kümesteki hayvanlardan birini ve yaşam sürelerini seçerek simülasyonu başlatabilirsiniz.

Başlangıçta bir dişi ve bir erkek üye ön tanımlı olarak kümese eklenmiştir.

Ayarlar dosyasından;

Her dişinin bir seferde kaç yeni yavru doğuracagı 

Doğumlardaki erkek dişi oranı,

Dişi doğurganlık süresi, 

Dişi ve erkek yaşam süreleri değiştirilebilir.

Kümese yeni hayvanlar ayarlar dosyasındaki model üzerinden eklenebilir.

Hayvanlara ait yeni detaylar eklenmek isterse modellerde ve kurguda değişiklik yapmak gerekir.


Kullanılan İşletim sistemi:
win 10, i7 ,16gb Ram, 2.40 Ghz 

Kullanıcı tarafından girilen yasam döngü girdilerin göre çalışma süreleri:

10 => 25,0926 milisaniye

20=> 52,5915

30=>669,9354

35=>10050,84

37=>49195,85